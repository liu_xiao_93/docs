# 网络支持

<a href="https://gitee.com/mindspore/docs/tree/master/docs/source_zh_cn/network_list.md" target="_blank"><img src="./_static/logo_source.png"></a>

## Model Zoo

|  领域 | 子领域    | 网络                                   | Ascend | GPU | CPU 
|:------   |:------| :-----------                               |:------   |:------  |:-----
|计算机视觉（CV） | 图像分类（Image Classification）  | [AlexNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/alexnet/src/alexnet.py)          |  Supported |  Supported | Doing
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [GoogleNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/googlenet/src/googlenet.py)                                               |  Supported     | Doing | Doing
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [LeNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/lenet/src/lenet.py)              |  Supported |  Supported | Supported
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [ResNet-50](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/resnet/src/resnet.py)          |  Supported |  Supported | Doing
|计算机视觉（CV）  | 图像分类（Image Classification）  | [ResNet-101](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/resnet/src/resnet.py)                                              |  Supported |Doing | Doing
|计算机视觉（CV）  | 图像分类（Image Classification）  | [ResNext50](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/resnext50/src/image_classification.py)                                             |  Supported |Doing | Doing
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [VGG16](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/vgg16/src/vgg.py)                |  Supported |  Doing | Doing
| 计算机视觉（CV）  | 移动端图像分类（Mobile Image Classification）<br>目标检测（Image Classification）<br>语义分割（Semantic Tegmentation）  | [MobileNetV2](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/mobilenetv2/src/mobilenetV2.py)                                            |  Supported |  Supported | Doing
| 计算机视觉（CV）  | 移动端图像分类（Mobile Image Classification）<br>目标检测（Image Classification）<br>语义分割（Semantic Tegmentation）  | [MobileNetV3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/mobilenetv3/src/mobilenetV3.py)                                            |  Doing |  Supported | Doing
|计算机视觉（CV）  | 目标检测（Targets Detection）  | [SSD](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/ssd/src/ssd.py)                   |  Supported |Doing | Doing
| 计算机视觉（CV）  | 目标检测（Targets Detection）  | [YoloV3-ResNet18](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/yolov3_resnet18/src/yolov3.py)         |  Supported |  Doing | Doing
| 计算机视觉（CV）  | 目标检测（Targets Detection）  | [FasterRCNN](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/official/cv/faster_rcnn/src/FasterRcnn)         |  Supported |  Doing | Doing
| 计算机视觉（CV） | 语义分割（Semantic Segmentation）  | [DeeplabV3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/cv/deeplabv3/src/deeplabv3.py)                                      |  Supported |  Doing | Doing
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [BERT](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/nlp/bert/src/bert_model.py)                                          |  Supported |  Doing | Doing
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [Transformer](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/nlp/transformer/src/transformer_model.py)                                          |  Supported |  Doing | Doing
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [SentimentNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/nlp/lstm/src/lstm.py)                                          |  Doing |  Supported | Supported
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [MASS](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/nlp/mass/src/transformer)                                          |  Supported |  Doing | Doing
| 推荐（Recommender） | 推荐系统、点击率预估（Recommender System, CTR prediction）  | [DeepFM](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/recommend/deepfm/src/deepfm.py)                                          |  Supported |  Doing | Doing
| 推荐（Recommender） | 推荐系统、搜索、排序（Recommender System, Search ranking）  | [Wide&Deep](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/recommend/wide_and_deep/src/wide_and_deep.py)                                          |  Supported |  Doing | Doing
| 图神经网络（GNN） | 文本分类（Text Classification）  | [GCN](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/gnn/gcn/src/gcn.py)                                          |  Supported |  Doing | Doing
| 图神经网络（GNN） | 文本分类（Text Classification）  | [GAT](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/official/gnn/gat/src/gat.py)                                          |  Supported |  Doing | Doing


## 预训练模型
建设中，即将上线
