# MindInsight Installation and Common Commands

[comment]: <> (This document contains Hands-on Tutorial Series. Gitee does not support display. Please check tutorials on the official website)

<video id="video7" autoplay controls width="1200px" height="600px" poster="https://mindspore-website.obs.cn-north-4.myhuaweicloud.com:443/teaching_video/cover/%E6%89%8B%E6%8A%8A%E6%89%8B%E7%B3%BB%E5%88%97/MI-1%E8%AF%A6%E6%83%85%E9%A1%B5.png">
<source id="mp47" src="https://mindspore-website.obs.cn-north-4.myhuaweicloud.com:443/teaching_video/video/MindInsight%E5%AE%89%E8%A3%85%E4%B8%8E%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4.mp4" type="video/mp4">
</video>

**Install now**: <https://www.mindspore.cn/install/en>

**More commands**: <https://www.mindspore.cn/tutorial/en/master/advanced_use/mindinsight_commands.html>